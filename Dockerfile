FROM openjdk:8
EXPOSE 8080
ADD /target/formcreator-0.0.1-SNAPSHOT.jar formcreator-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","formcreator-0.0.1-SNAPSHOT.jar"]

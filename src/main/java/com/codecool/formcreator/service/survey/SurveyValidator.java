package com.codecool.formcreator.service.survey;


import com.codecool.formcreator.dto.survey.SurveyDto;
import com.codecool.formcreator.service.Validator;
import org.springframework.stereotype.Component;

@Component
public class SurveyValidator implements Validator<SurveyDto> {
    @Override
    public boolean isValid(SurveyDto dto) {
        return true;
    }
}

package com.codecool.formcreator.service;

public interface Translator<Dto, Model> {

    Dto modelToDto(Model model);
    Model dtoToModel(Dto dto);
}

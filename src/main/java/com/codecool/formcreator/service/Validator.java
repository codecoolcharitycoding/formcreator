package com.codecool.formcreator.service;

public interface Validator<T> {
    boolean isValid(T dto);
}

package com.codecool.formcreator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {
    @RequestMapping({"/", "/survey*", "/choose*",  "/stats*"})
    public String index() {
        return "forward:/index.html";
    }
}

package com.codecool.formcreator.repository;

import com.codecool.formcreator.model.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface QuestionRepository extends CrudRepository<Question, Long> {
}
package com.codecool.formcreator.repository;

import com.codecool.formcreator.model.Property;
import org.springframework.data.repository.CrudRepository;

public interface PropertyRepository extends CrudRepository<Property, Long> {
}

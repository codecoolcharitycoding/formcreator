package com.codecool.formcreator.repository;

import com.codecool.formcreator.model.Answer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnswerRepository extends CrudRepository<Answer, Long> {

    List<Answer> getAnswersByContact_Id(Long contactId);
    List<Answer> getAnswersByQuestionSurveyId(Long surveyId);
    List<Answer> getAnswersByQuestionSurveyIdAndContactId(Long surveyId, Long contactId);

}
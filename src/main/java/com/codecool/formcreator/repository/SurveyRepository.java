package com.codecool.formcreator.repository;

import com.codecool.formcreator.model.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

public interface SurveyRepository extends CrudRepository<Survey, Long> {

}

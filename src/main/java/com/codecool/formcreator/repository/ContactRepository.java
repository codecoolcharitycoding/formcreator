package com.codecool.formcreator.repository;

import com.codecool.formcreator.model.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {

    Optional<Contact> findById(Long id);
    Optional<Contact> findByName(String name);

}

package com.codecool.formcreator.repository;

import com.codecool.formcreator.model.AvailableAnswer;
import com.codecool.formcreator.model.Contact;
import org.springframework.data.repository.CrudRepository;

public interface AvailableAnswerRepository extends CrudRepository<AvailableAnswer, Long> {
}
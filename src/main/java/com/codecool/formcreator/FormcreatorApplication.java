package com.codecool.formcreator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormcreatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormcreatorApplication.class, args);
	}
}

import { Injectable } from '@angular/core';
import {Survey} from '../../models/survey';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  static ENDPOINT_URL = '/fc/api/surveys/';

  constructor(private http: HttpClient) {
  }

  persist(survey: Survey) {
    console.log(JSON.stringify(survey));
    return this.http.post(SurveyService.ENDPOINT_URL, survey);
  }

  get(id: number): Observable<Survey> {
    return this.http.get<Survey>(SurveyService.ENDPOINT_URL + id);
  }

  getAll() {
    console.log("geting all surveys");
    return this.http.get<Survey[]>(SurveyService.ENDPOINT_URL);
  }
}

import {Question} from './question';

export class Schema {
  fields: Question[];
  constructor() {
    this.fields = [];
  }
}

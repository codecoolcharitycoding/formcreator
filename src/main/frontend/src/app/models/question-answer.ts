export class QuestionAnswer {
  key: string;
  values: string[];

  constructor(key, values) {
    this.key = key;
    this.values = values;
  }
}

export enum QuestionType {
  input = 'Text Input',
  textarea = 'Textarea Input',
  radiobox = 'Single choice',
  checkbox = 'Multiple choice'
}

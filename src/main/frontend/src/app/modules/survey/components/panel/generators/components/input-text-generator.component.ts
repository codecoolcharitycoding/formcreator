import {Component} from '@angular/core';
import {SimpleGeneratorComponent} from "./abstract/simple-generator.component";


@Component({
  selector: 'input-text-generator',
  templateUrl: '../templates/input-text-generator.component.html',
})
export class InputTextGeneratorComponent extends SimpleGeneratorComponent {

}

import {Component} from '@angular/core';
import {MutliplePreviewComponent} from "./abstract/mutliple-preview.component";

@Component({
  selector: 'checkbox-preview',
  templateUrl: '../templates/checkbox-preview.component.html',
})
export class CheckboxPreviewComponent extends MutliplePreviewComponent {
}

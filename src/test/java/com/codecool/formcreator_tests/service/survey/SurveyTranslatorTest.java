//package com.codecool.formcreator_tests.service.survey;
//
//import com.codecool.formcreator.dto.survey.FieldDto;
//import com.codecool.formcreator.dto.survey.SurveyDto;
//import com.codecool.formcreator.model.AvailableAnswer;
//import com.codecool.formcreator.model.Property;
//import com.codecool.formcreator.model.Question;
//import com.codecool.formcreator.model.Survey;
//import com.codecool.formcreator.service.survey.SurveyTranslator;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.Ignore;
//import org.junit.Test;
//
//import java.util.HashSet;
//import java.util.Map;
//import java.util.Set;
//import java.util.TreeSet;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//public class SurveyTranslatorTest {
//
//    @Test
//    public void testModelToDtoTranslationName() {
//        String surveyName = "Ankieta 1";
//
//        Survey survey = new Survey(surveyName, new HashSet<>());
//
//        SurveyTranslator translator = new SurveyTranslator();
//        SurveyDto surveyDto = translator.modelToDto(survey);
//
//        assertEquals(surveyName, surveyDto.getName());
//    }
//
//    @Test
//    public void testModelToDtoTranslationFieldType() {
//        String type = "pytanie";
//
//        Survey survey = new Survey();
//
//        Set<Question> questions = new HashSet<>();
//        Question question = new Question();
//        question.setSurvey(survey);
//        question.setType(type);
//        questions.add(question);
//        survey.setQuestions(questions);
//
//        SurveyTranslator translator = new SurveyTranslator();
//        SurveyDto surveyDto = translator.modelToDto(survey);
//
//        FieldDto field = surveyDto.getSchema().getFields().get(0);
//
//        assertEquals(type, field.getType());
//
//    }
//
//
//    @Test
//    public void testModelToDtoTranslationFieldLabel() {
//        String label = "pytanie";
//
//        Survey survey = new Survey();
//
//        Set<Question> questions = new HashSet<>();
//        Question question = new Question();
//        question.setSurvey(survey);
//        question.setDescription(label);
//        questions.add(question);
//        survey.setQuestions(questions);
//
//        SurveyTranslator translator = new SurveyTranslator();
//        SurveyDto surveyDto = translator.modelToDto(survey);
//
//        FieldDto field = surveyDto.getSchema().getFields().get(0);
//
//        assertEquals(label, field.getLabel());
//
//    }
//
//    @Test
//    public void testModelToDtoTranslationFieldProperty() {
//        String property = "dlugi";
//
//        Survey survey = new Survey();
//
//        Set<Question> questions = new HashSet<>();
//        Question question = new Question();
//
//        Set<Property> properties = new HashSet<>();
//        properties.add(new Property(question, property, ""));
//
//        question.setProperties(properties);
//        question.setSurvey(survey);
//        questions.add(question);
//        survey.setQuestions(questions);
//
//        SurveyTranslator translator = new SurveyTranslator();
//        SurveyDto surveyDto = translator.modelToDto(survey);
//
//        FieldDto field = surveyDto.getSchema().getFields().get(0);
//        Map<String, String> propertiesMap = field.getProperties();
//
//        assertTrue(propertiesMap.containsKey(property));
//
//    }
//
//    @Test
//    public void testModelToDtoTranslationFieldPropertyValue() {
//        String property = "dlugi";
//        String value = "tak";
//
//        Survey survey = new Survey();
//
//        Set<Question> questions = new HashSet<>();
//        Question question = new Question();
//
//        Set<Property> properties = new HashSet<>();
//        properties.add(new Property(question, property, value));
//
//        question.setProperties(properties);
//        question.setSurvey(survey);
//        questions.add(question);
//        survey.setQuestions(questions);
//
//        SurveyTranslator translator = new SurveyTranslator();
//        SurveyDto surveyDto = translator.modelToDto(survey);
//
//        FieldDto field = surveyDto.getSchema().getFields().get(0);
//        Map<String, String> propertiesMap = field.getProperties();
//
//        assertEquals(value, propertiesMap.get(property));
//
//    }
//
//    @Test
//    public void testModelToDtoTranslationFieldAvailableAnswers() {
//        String value = "scala";
//
//        Survey survey = new Survey();
//
//        Set<Question> questions = new HashSet<>();
//        Question question = new Question();
//
//        Set<AvailableAnswer> availableAnswers = new HashSet<>();
//        availableAnswers.add(new AvailableAnswer(question, value));
//        question.setAvailableAnswers(availableAnswers);
//        question.setSurvey(survey);
//        questions.add(question);
//        survey.setQuestions(questions);
//
//        SurveyTranslator translator = new SurveyTranslator();
//        SurveyDto surveyDto = translator.modelToDto(survey);
//
//        FieldDto field = surveyDto.getSchema().getFields().get(0);
//
//        assertTrue(field.getValues().contains(value));
//
//    }
//
//    @Test
//    @Ignore
//    public void testModelToJsonTranslation() throws JsonProcessingException {
//        Survey survey = createSampleSurveyModel();
//
//        String expectedJson = "{\"name\":\"Ankieta 1\",\"schema\":{\"fields\":[{\"label\":\"languages\",\"type\":\"radio\",\"model\":\"\",\"values\":[\"scala\",\"java\"]},{\"label\":\"Name\",\"type\":\"input\",\"model\":\"\",\"required\":true}]}}";
//
//        SurveyTranslator surveyTranslator = new SurveyTranslator();
//        SurveyDto surveyDto = surveyTranslator.modelToDto(survey);
//
//        ObjectMapper mapper = new ObjectMapper();
//        String givenJson = mapper.writeValueAsString(surveyDto);
//
//        assertEquals(expectedJson, givenJson);
//    }
//
//
//    private Survey createSampleSurveyModel() {
//        Survey survey = new Survey();
//        survey.setName("Ankieta 1");
//
//        Set<Question> questions = new TreeSet<>();
//
//        Question q1 = new Question();
//        q1.setType("input");
//        q1.setDescription("Name");
//        q1.setSurvey(survey);
//        q1.setAvailableAnswers(new TreeSet<>());
//        Set<Property> properties = new TreeSet<>();
//        properties.add(new Property(q1, "required", "true"));
//        q1.setProperties(properties);
//
//        Question q2 = new Question();
//        q2.setType("radio");
//        q2.setDescription("languages");
//        q2.setSurvey(survey);
//        Set<AvailableAnswer> availableAnswers = new TreeSet<>();
//        availableAnswers.add(new AvailableAnswer(q2, "java"));
//        availableAnswers.add(new AvailableAnswer(q2, "scala"));
//        q2.setAvailableAnswers(availableAnswers);
//        q2.setProperties(new TreeSet<>());
//
//        questions.add(q1);
//        questions.add(q2);
//
//        survey.setQuestions(questions);
//
//        return survey;
//    }
//
//}
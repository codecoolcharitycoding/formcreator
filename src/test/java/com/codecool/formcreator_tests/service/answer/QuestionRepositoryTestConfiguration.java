//package com.codecool.formcreator_tests.service.answer;
//
//import com.codecool.formcreator.repository.QuestionRepository;
//import org.mockito.Mockito;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.context.annotation.Profile;
//
//@Profile("test")
//@Configuration
//public class QuestionRepositoryTestConfiguration {
//    @Bean
//    @Primary
//    public QuestionRepository contactRepository() {
//        return Mockito.mock(QuestionRepository.class);
//    }
//}
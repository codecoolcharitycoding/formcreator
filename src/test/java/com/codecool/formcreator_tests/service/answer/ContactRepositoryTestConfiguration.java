//package com.codecool.formcreator_tests.service.answer;
//
//import com.codecool.formcreator.repository.ContactRepository;
//import org.mockito.Mockito;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.context.annotation.Profile;
//
//@Profile("test")
//@Configuration
//public class ContactRepositoryTestConfiguration {
//    @Bean
//    @Primary
//    public ContactRepository contactRepository() {
//        return Mockito.mock(ContactRepository.class);
//    }
//}

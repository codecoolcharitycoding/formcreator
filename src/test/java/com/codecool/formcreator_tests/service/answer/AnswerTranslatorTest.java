//package com.codecool.formcreator_tests.service.answer;
//
//import com.codecool.formcreator.FormcreatorApplication;
//import com.codecool.formcreator.dto.answer.AnswerDto;
//import com.codecool.formcreator.model.Answer;
//import com.codecool.formcreator.model.Contact;
//import com.codecool.formcreator.model.Question;
//import com.codecool.formcreator.model.Survey;
//import com.codecool.formcreator.repository.ContactRepository;
//import com.codecool.formcreator.repository.QuestionRepository;
//import com.codecool.formcreator.service.answer.AnswerTranslator;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.util.*;
//import java.util.function.Predicate;
//
//import static org.junit.Assert.*;
//import static org.mockito.ArgumentMatchers.booleanThat;
//import static org.mockito.Mockito.when;
//
//@ActiveProfiles("test")
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = FormcreatorApplication.class)
//public class AnswerTranslatorTest {
//
//    @Autowired
//    AnswerTranslator underTest;
//
//    @MockBean
//    ContactRepository contactRepository;
//
//    @MockBean
//    QuestionRepository questionRepository;
//
//    Contact contact;
//    Survey survey;
//    Question question1;
//    Question question2;
//    Answer answer1;
//    Answer answer2;
//
//    @Before
//    public void setup() {
//        contact = new Contact();
//        contact.setId(0L);
//
//        survey = new Survey();
//        survey.setId(0L);
//
//        question1 = new Question();
//        question1.setSurvey(survey);
//
//        question2 = new Question();
//        question2.setSurvey(survey);
//
//        answer1 = new Answer();
//        answer1.setContact(contact);
//        answer1.setQuestion(question1);
//        answer1.setValue("a");
//
//        answer2 = new Answer();
//        answer2.setContact(contact);
//        answer2.setQuestion(question2);
//        answer2.setValue("b");
//
//        when(contactRepository.findById(0L)).thenReturn(Optional.of(contact));
//        when(questionRepository.findById(0L)).thenReturn(Optional.of(question1));
//        when(questionRepository.findById(1L)).thenReturn(Optional.of(question2));
//    }
//
//    @Test
//    public void testDtoToModel_answersAmount() {
//        AnswerDto dto = new AnswerDto();
//        dto.setContactId(0);
//        dto.setSurveyId(0);
//        Map<String, List<String>> responses = new HashMap<>();
//        List<String> responseA = new ArrayList() {{
//            add("a");
//        }};
//        responses.put("1-0", responseA);
//        List<String> responseB = new ArrayList() {{
//            add("b");
//        }};
//        responses.put("2-1", responseB);
//        dto.setResponses(responses);
//
//        assertEquals("answer amount",2, underTest.dtoToModel(dto).size());
//    }
//
//    @Test
//    public void testDtoToModel_containsValues() {
//        AnswerDto dto = new AnswerDto();
//        dto.setContactId(0);
//        dto.setSurveyId(0);
//        Map<String, List<String>> responses = new HashMap<>();
//        List<String> responseA = new ArrayList() {{
//            add("a");
//        }};
//        List<String> responseB = new ArrayList() {{
//            add("b");
//        }};
//        responses.put("1-0", responseA);
//        responses.put("2-1", responseB);
//        dto.setResponses(responses);
//
//        assertTrue(underTest.dtoToModel(dto).contains(answer1));
//        assertTrue(underTest.dtoToModel(dto).contains(answer2));
//    }
//
//    @Test
//    public void testModelToDto() {
//
//        List<Answer> answers = new ArrayList() {{
//            add(answer1);
//            add(answer2);
//        }};
//        List<AnswerDto> answersDto = underTest.modelToDto(answers);
//
//        for (AnswerDto answerDto: answersDto) {
//
//            for (Map.Entry<String, List<String>> responseEntry: answerDto.getResponses().entrySet()) {
//
//                for (String value: responseEntry.getValue()) {
//                    assertTrue(answersContainsAnswer(answers, value));
//                }
//            }
//        }
//    }
//
//    private boolean answersContainsAnswer(List<Answer> answers, String value) {
//
//        for (Answer answer: answers) {
//            if (answer.getValue().equals(value)) return true;
//        }
//
//        return false;
//    }
//}

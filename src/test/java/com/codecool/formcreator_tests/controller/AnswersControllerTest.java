//package com.codecool.formcreator_tests.controller;
//
//import com.codecool.formcreator.FormcreatorApplication;
//import com.codecool.formcreator.model.Answer;
//import com.codecool.formcreator.model.Contact;
//import com.codecool.formcreator.model.Question;
//import com.codecool.formcreator.model.Survey;
//import com.codecool.formcreator.repository.AnswerRepository;
//import com.codecool.formcreator.repository.ContactRepository;
//import com.codecool.formcreator.repository.QuestionRepository;
//import com.codecool.formcreator.repository.SurveyRepository;
//import org.junit.Before;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = FormcreatorApplication.class)
//@AutoConfigureMockMvc
//@TestPropertySource(locations = "resources/application-integrationtest.properties")
//class AnswersControllerTest {
//
//    @Autowired
//    private MockMvc mvc;
//
//    @Autowired
//    private AnswerRepository answerRepository;
//
//    @Autowired
//    private ContactRepository contactRepository;
//
//    @Autowired
//    private SurveyRepository surveyRepository;
//
//    @Autowired
//    private QuestionRepository questionRepository;
//
//    @Before
//    public void setUp() {
//        Contact c1 = new Contact();
//        Contact c2 = new Contact();
//        c1.setName("tosia");
//        c2.setName("wera");
//        contactRepository.save(c1);
//        contactRepository.save(c2);
//
//        Survey s1 = new Survey();
//        Survey s2 = new Survey();
//        s1.setName("s1");
//        s2.setName("s2");
//        surveyRepository.save(s1);
//        surveyRepository.save(s2);
//
//        Question[] questions = new Question[6];
//        for (int i =0; i<6; i++) {
//            questions[i] = new Question();
//            questions[i].setDescription(String.format("czy %d?", i));
//            questions[i].setLineup(i<3?i:i%3);
//            questions[i].setSurvey(i<3?s1:s2);
//            questions[i].setType("input");
//            questionRepository.save(questions[i]);
//
//        }
//
//    }
//
//}